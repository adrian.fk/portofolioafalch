import MenuView from "../View/MenuView.js";
import Links from "../../Links/Links.js";
import Falcon from "../../FalconUtils/Falcon.js";
import {MAIN_STAGE_ID} from "../../FalconUtils/Falcon.js";
import Log from "../../FalconUtils/Log.js";

const TAG = "Menu Controller";

export default class MenuController {

    constructor() {
        this.stageManager = Falcon.StageManager();
        this.stageManager.setStageByID(MAIN_STAGE_ID);

        return this;
    }

    #registerListeners(){
        Log.d(TAG, "Registering Menu listeners");


        document.getElementById("aboutBtn").onclick = (ev) => new MenuController().navToAbout();

        document.getElementById("projectsBtn").onclick = (ev) => new MenuController().navToProjects();

        document.getElementById("contactBtn").onclick = (ev) => new MenuController().navToContact();

        document.getElementById("linksBtn").onclick = (ev) => new MenuController().navToLinks();
    }

    navToHome() {
        const menuView = new MenuView();
        menuView.navigateTo(this.#registerListeners);
    }

    navToAbout() {
        alert("about")
    }

    navToProjects() {
        alert("hi");
    }

    navToLinks() {
        Links.execute();
    }

    navToContact() {
        alert("contact")
    }
}
