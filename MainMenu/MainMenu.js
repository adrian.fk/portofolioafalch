import MenuController from "./Controller/MenuController.js";

export default class MainMenu {

    static execute() {
        this.menuController = new MenuController();
        this.menuController.navToHome();
    }

}