import BaseView from "../../BaseView.js";
import LinksController from "../../Links/Controller/LinksController.js";
import Falcon from "../../FalconUtils/Falcon.js";
import Log from "../../FalconUtils/Log.js";
import Projects from "../../Projects/Projects.js";

const TAG = "MenuView";

const cssDomain = "MainMenu/view/MenuView.css";
const htmlDomain = "./MainMenu/view/MenuView.html";

export default class MenuView extends BaseView{

    constructor() {
        super(htmlDomain, cssDomain);
    }


    navigateTo(callback) {
        const stageManager = Falcon.StageManager();
        const wrapperFragment = Falcon.newFragment("fragmentsWrapper", stageManager.stage.stage);
        Log.d(TAG, "Managers established");

        let stackCounter = 0;

        stageManager.toggleFade();

        const menuFragment = Falcon.newFragment("menu")
            .withHtmlFile(
                htmlDomain,
                html => {
                    stageManager.importCSS(cssDomain);
                    Log.d(TAG, "Menu Fragment established");
                    checkIfStackIsFilled();
                }
            );

        const linksFragment = Falcon.newFragment("links")
            .withHtmlFile(
                "Links/view/LinksView.html",
                html => {
                    stageManager.importCSS("Links/view/LinksView.css");
                    Log.d(TAG, "Links Fragment established");
                    checkIfStackIsFilled();
                }
            );

        const projectFragment = Falcon.newFragment("_projects")
            .withHtmlFile(
                "Projects/controller/presenter/view/ProjectsView.html",
                html => {
                    stageManager.importCSS("Projects/controller/presenter/view/ProjectsView.css")
                    stageManager.importCSS("Projects/controller/presenter/view/ProjectView.css")
                    Log.d(TAG, "Projects Fragment established")
                    checkIfStackIsFilled();
                }
            )

        wrapperFragment.addFragment(menuFragment);
        wrapperFragment.addFragment(linksFragment);
        wrapperFragment.addFragment(projectFragment);

        function checkIfStackIsFilled() {
            if (++stackCounter === wrapperFragment.getNumContainingFragments()) {
                Log.d(TAG, "Commiting html stack");

                setTimeout(
                    function () {
                        wrapperFragment.commit(true, callback);
                        stageManager.scrollToTop();
                        stageManager.toggleFade();
                        const c = new LinksController();
                        c.registerListeners();
                        Projects.execute();
                    },
                    300
                )
            }
        }
    }

}
