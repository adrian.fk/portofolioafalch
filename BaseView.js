import Falcon from "./FalconUtils/Falcon.js";

export default class BaseView {

    constructor(htmlPath, cssPath) {
        this.cssPath = cssPath;
        this.htmlPath = htmlPath;
        Falcon.StageManager().setStageByID("main");
    }

    setHtmlPath(htmlPath) {
        this.htmlPath = htmlPath;
    }

    setCssPath(cssPath) {
        this.cssPath = cssPath;
    }

    getHtmlDomain() {
        return this.htmlPath;
    }

    getCssDomain() {
        return this.cssPath;
    }

    navigateTo(callback) {
        Falcon.StageManager().navigateToHtml(this.htmlPath, this.cssPath, callback);
    }
}