import AboutPresenter from "./presenter/AboutPresenter";

export default
class AboutController {

    constructor() {
        if (!AboutController.INSTANCE) {
            AboutController.INSTANCE = this;
        }
        return AboutController.INSTANCE;
    }

    static getInstance() {
        return new AboutController();
    }

    init() {
        const presenter = AboutPresenter.getInstance();
        presenter.init();
    }

    #registerListeners() {

    }
}