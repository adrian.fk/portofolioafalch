export default
class AboutPresenter {

    constructor() {
        if (!AboutPresenter.INSTANCE) {
            AboutPresenter.INSTANCE = this;
        }
        return AboutPresenter.INSTANCE;
    }

    static getInstance() {
        return new AboutPresenter();
    }

    init() {

    }

    registerOnClickListener(tagId, callback) {

    }
}