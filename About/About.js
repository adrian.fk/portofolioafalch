import AboutController from "./AboutController.js";

export default
class About {
    static execute() {
        this.controller = AboutController.getInstance();
        this.controller.init();
    }
}