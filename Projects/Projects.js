import ProjectsController from "./controller/ProjectsController.js";

export default class Projects {
    static execute() {
        new ProjectsController().init();
    }
}