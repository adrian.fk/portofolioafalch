export default
class ProjectView {

    render(
        projectId = -1,
        projectName = "",
        description = "",
        gitLink = "",
        pictures = []) {

        return ""
        +"<div class='projectBackground'>"
        +   "<div class='project'>"
        +      "<div class='project__image'>"
        +          `<img src='${pictures[0]}' alt='' /> `
        +      "</div>"
        +      "<div class='project__info'>"
        +          "<div class='info__title'>"
        +              `<h2>${projectName}</h2>`
        +          "</div>"
        +          "<hr />"
        +          "<div class='info__link'>"
        +              `<a href='${gitLink}' target='_blank'><h4>${gitLink}</h4></a>`
        +          "</div> "
        +          "<div class='info__description'>"
        +              `<p>${description}</p>`
        +          "</div>"
        +      "</div>"
        +   "</div>"
        +"</div>"
    }
}