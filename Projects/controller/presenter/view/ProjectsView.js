import BaseView from "../../../../BaseView.js";
import ProjectView from "./ProjectView.js";
import Falcon from "../../../../FalconUtils/Falcon.js";

const htmlPath = "Projects/controller/presenter/view/ProjectsView.html";
const cssPath = "Projects/controller/presenter/view/ProjectsView.css";
const projectCssPath = "Projects/controller/presenter/view/ProjectsView.css";

export default class ProjectsView extends BaseView {

    constructor() {
        super(htmlPath, cssPath);
        this.projects = [];
        Falcon.StageManager().importCSS(cssPath);
        Falcon.StageManager().importCSS(projectCssPath);
    }

    getProjectHTML(project = {
        projectId: -1,
        projectName: "",
        description: "",
        gitLink: "",
        pictures: []
    }) {
        return new ProjectView()
            .render(project.projectId, project.projectName, project.description, project.gitLink, project.pictures);
    }

    addProjectToDeployStack(project = {
        projectId: -1,
        projectName: "",
        description: "",
        gitLink: "",
        pictures: []
    }) {
        this.projects.push(this.getProjectHTML(project));
    }

    deployProjectView(callback) {
        let html = "";
        for (let i = 0; i < this.projects.length; i++) {
            html += this.projects[i];
        }
        Falcon.fillElementWithHtml("ProjectsView", html,  callback);
    }
}