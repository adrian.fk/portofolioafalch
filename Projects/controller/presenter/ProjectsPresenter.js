import ProjectsView from "./view/ProjectsView.js";

export default class ProjectsPresenter {

    init() {
        this.view = new ProjectsView();
    }

    presentProjects(projects = [], callback) {
        for (let i = 0; i < projects.length; i++) {
            this.view.addProjectToDeployStack(projects[i]);
        }
        this.view.deployProjectView(callback);
    }
}