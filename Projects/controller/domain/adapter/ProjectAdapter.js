import Project from "../Model/Project.js";

export default
class ProjectAdapter {
    static mapToViewModel(project = new Project()) {
        return {
            projectId: project.getProjectId(),
            projectName: project.getProjectName(),
            description: project.getDescription(),
            gitLink: project.getLink(),
            pictures: project.getPictures(),
        }
    }

    static mapToModel(project = {
        projectId: -1,
        projectName: "",
        description: "",
        gitLink: "",
        pictures: []
    }) {
        return new Project(project.projectId, project.projectName, project.description, project.gitLink, project.pictures);
    }
}