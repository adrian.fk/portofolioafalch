import Project from "../Model/Project.js";

const projectName = "Facebook Clone";
const link = "https://gitlab.com/adrian.fk/fb-clone"
const description = "Facebook Clone: <a href='https://facebook-clone-e8482.web.app/' target='_blank'>" +
    "https://facebook-clone-e8482.web.app/</a> \n<br />" +
    "\n<br />" +
    "\n<br />The main purpose of this clone was to display capabilities with front-end programming, using a very popular " +
    "JavaScript framework, developed by facebook, called React. In this project it has also been displayed capabilities working with a simple " +
    "database scheme and a authentication process using an external authentication module like Google Authentication." +
    "\n<br />" +
    "\n<br />" +
    "\n<br />" +
    "Applied technologies:\n<br />" +
    "\n<br />" +
    "React, REDUX pattern (React HOOKS), Material UI, Flexbox, Firebase´s Firestone real-time DB, Firebase Hosting, Firebase Google Authentication";

const pictures = ["https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Facebook_Logo_%282019%29.png/1200px-Facebook_Logo_%282019%29.png"];

export default
class fbClone extends Project {
    constructor(projectId) {
        super(projectId, projectName, description, link, pictures);
    }
}