export default
class Project {


    constructor(projectId, projectName, description, link, pictures) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.description = description;
        this.link = link;
        this.pictures = pictures;
    }

    setProjectId(newProjectId) {
        this.projectId = newProjectId;
    }

    getProjectId() {
        return this.projectId;
    }

    setProjectName(nProjectName) {
        this.projectName = nProjectName;
    }

    getProjectName() {
        return this.projectName;
    }

    setDescription(nDescription) {
        this.description = nDescription;
    }

    getDescription() {
        return this.description;
    }

    setLink(nLink) {
        this.link = nLink;
    }

    getLink() {
        return this.link;
    }

    setPictures(nPictures) {
        this.pictures = nPictures;
    }

    getPictures() {
        return this.pictures;
    }
}