import ProjectsPresenter from "./presenter/ProjectsPresenter.js";
import ProjectAdapter from "./domain/adapter/ProjectAdapter.js";
import fbClone from "./domain/content/fbClone.js";

export default class ProjectsController {
    init() {
        this.presenter = new ProjectsPresenter();
        this.presenter.init();
        let projects = [];
        projects.push(
            ProjectAdapter.mapToViewModel(new fbClone(projects.length))
        )

        this.presenter.presentProjects(projects, () => this.#registerListeners());
    }

    #registerListeners() {

    }
}