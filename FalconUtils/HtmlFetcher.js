/**
 *  The HTML Fetcher does as the name describes.
 *  It fetches the HTML page asynchronously and returning it as a String containing the HTML through passing this
 *  String onto the callback method, specified either with the constructor or with the fetching method itself.
 *
 */

export default class HtmlFetcher {

    constructor(callback = false) {
        this.callback = callback;
    }

    setCallback(callback) {
        this.callback = callback;
    }

    fetchAsync(resourcePath, callback) {
        const xmlHttp = new XMLHttpRequest();
        const htmlFetcher = this;

        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
                if (htmlFetcher.callback) {
                    htmlFetcher.callback(xmlHttp.responseText);
                }
                else {
                    callback(xmlHttp.responseText);
                }
            }
        };

        xmlHttp.open("GET", resourcePath, true);
        xmlHttp.send(null)
    }
}