import {requiredParameter} from "../Tools.js";
import Log from "../Log.js";

const TAG = "Stage";

export default
class Stage {

    constructor(stageId) {
        if (!stageId) stageId = "main";
        if (typeof stageId === typeof " ") {
            this.stage = this.#getStage(stageId);
            this.stageId = stageId;
        }
        else {
            Log.d(TAG, "Stage constructor was not provided correct format (String) in it's parameter")
        }
    }

    #getStage(stageId) {
        let stage = document.getElementById(stageId);
        if (!stage) {
            stage = document.getElementsByClassName(stageId);
            if (!stage) {
                stage = document.getElementsByName(stageId);
                if (!stage) {
                    stage = document.getElementsByTagName(stageId);
                }
            }
        }
        this.foundSomeStage = !!stage;
        return stage;
    }

    setStage(stageId = requiredParameter(TAG, "stageId", "setStage")) {
        const stageCache = this.stage;
        this.stage = this.#getStage(stageId);
        if (this.foundSomeStage) {
            this.stageId = stageId;
        }
        else {
            this.stage = stageCache;
            Log.d(TAG, "Could not set stage based on given identificator. Reverted to previous stage.");
        }
    }

    setStageHtml(html = requiredParameter(TAG, "html", "setStageHtml") || "") {
        if (this.foundSomeStage) this.stage.innerHTML = html;
    }

    scrollToTop() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    getDomStage() {
        return this.stage;
    }
}