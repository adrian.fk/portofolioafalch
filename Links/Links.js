import LinksController from "./Controller/LinksController.js";

export default class Links {

    static execute() {
        this.controller = new LinksController();
        this.controller.initiate();
    }
}