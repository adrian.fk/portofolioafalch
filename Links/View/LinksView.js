import BaseView from "../../BaseView.js";

const cssDomain = "Links/view/LinksView.css";
const htmlDomain = "Links/view/LinksView.html";

export default class LinksView extends BaseView{

    constructor() {
        super(htmlDomain, cssDomain);
    }
}