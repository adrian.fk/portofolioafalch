import LinksView from "../View/LinksView.js";
import MenuController from "../../MainMenu/Controller/MenuController.js";
import Log from "../../FalconUtils/Log.js";

const TAG = "LinksController";

export default class LinksController {

    constructor() {
        this.view = new LinksView();
    }

    registerListeners() {
        Log.d(TAG, "Registering listeners");

        document.getElementById("links-backBtn").onclick = (ev) => new MenuController().navToHome();

    }

    initiate() {
        this.view.navigateTo(this.registerListeners);
    }
}